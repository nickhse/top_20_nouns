import json

import nltk
import requests
from bs4 import BeautifulSoup
from nltk.tokenize import word_tokenize

nltk.download('averaged_perceptron_tagger')
nltk.download('punkt')

OUTPUT_FILE = 'top_20_nouns.json'

url = 'http://hadoop.apache.org'

response = requests.get(url)
document = BeautifulSoup(response.text, 'html.parser')

list = []

for tag in document.find_all('p'):
    list.append(tag.text)

full_text = ''.join(list)

tokenize_text = word_tokenize(full_text)

cnt = nltk.Counter()

for (word, pos) in nltk.pos_tag(tokenize_text):
    if pos[:2] == 'NN':
        cnt[word] += 1

with open(OUTPUT_FILE, 'w') as output:
    output.write(json.dumps(cnt.most_common(20), sort_keys=True, indent=1))
